#!/usr/bin/env python3

from distutils.core import setup, Extension

mod = Extension('ptraceable', sources=['ptraceable.c'], libraries=[])

setup(name='Ptraceable', version='0.0',
    description='Import this module to call prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY).',
    ext_modules=[mod])
