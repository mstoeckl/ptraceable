#include <sys/prctl.h>
#include <Python.h>

static PyMethodDef Methods[] = {
    {NULL, NULL, 0, NULL} /* Sentinel */
};
    
static struct PyModuleDef module = {
    PyModuleDef_HEAD_INIT, "ptraceable",
    "Import this module to call prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY).",
    -1,
    Methods};

  
PyMODINIT_FUNC PyInit_ptraceable(void) {
    PyObject *p = PyModule_Create(&module);

    prctl(PR_SET_PTRACER, PR_SET_PTRACER_ANY);
    // prctl
    return p;
}